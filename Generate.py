from xml.dom import minidom

import numpy as np
from matplotlib.path import Path
import matplotlib.pyplot as plt
import matplotlib.patches as patches
import sys
import scipy.misc
import os
from PIL import Image

def parse_annotation(imName,objects):
    im = Image.open('images/'+imName+'.jpg')
    height,width = im.size
    xmldoc = minidom.parse('annotations/'+imName+'.xml')
    itemlist = xmldoc.getElementsByTagName('object')

    #all points
    all_cords = []
    for i in range(0,width):
        for j in range(0,height):
            all_cords.append([j,width-i])

    #256*256 for image
    img = np.zeros((width,height))
    #objects = {'stones': 1, 'slope': 2, 'fence': 3, 'bridge': 26, 'house': 5, 'snowy mountain': 17, 'pole': 7, 'plants': 46, 'trash can': 8, 'flock': 9, 'mill': 10, 'cloud': 11, 'ground': 12, 'mountain': 13, 'van': 14, 'sky': 16, 'snow': 6, 'machine': 18, 'field': 19, 'window': 20, 'hill': 21, 'animal': 22, 'valley': 23, 'lake': 24, 'urban valley': 25, 'mountains': 4, 'buildings': 28, 'door': 29, 'sea water': 31, 'trees': 33, 'water': 34, 'houses': 35, 'path': 36, 'sign': 37, 'bushes': 38, 'building': 27, 'street light': 39, 'stone': 40, 'land': 41, 'sand': 47, 'car': 48, 'river water': 42, 'plain': 43, 'tree': 44, 'rocks': 45, 'person': 15, 'tableland': 30, 'rock': 32, 'fence crop': 49, 'grass': 50, 'road': 51}

    #objects = {'sky':1,'grass':2,'valley':3,'mountain':4,'trees':5}
    for s in itemlist:
        nm = s.getElementsByTagName('name')

        poly = s.getElementsByTagName('polygon')
        points = poly[0].getElementsByTagName('pt')
        cords = []
        startpoint = []
        for point in points:
            x = point.getElementsByTagName('x')[0].childNodes[0].nodeValue
            y = point.getElementsByTagName('y')[0].childNodes[0].nodeValue
            y = width-int(y)
            cords.append([x,y])
            if(len(startpoint) == 0):
                startpoint = [x,y]
        cords.append(startpoint)#close the polygon
        cords = np.array(cords)
        path = Path(cords)
        if_contains = path.contains_points(np.array(all_cords))
        if_contains = if_contains.reshape((width,height))

        nm_lowercase = nm[0].childNodes[0].nodeValue.lower()
        #print(nm_lowercase)
        #print(nm_lowercase in objects)

        if(nm_lowercase in objects):
            img[if_contains] = objects[nm[0].childNodes[0].nodeValue]

    #scipy.misc.imsave('Parsed/'+imName+'.jpg', img)
    scipy.misc.toimage(img,  high=np.max(img), low=np.min(img)).save('Parsed/'+imName+'.png')


#read objects dictionary from object.txt
objects = {}
fo = open('object.txt')
for line in fo.readlines():
    terms = line.split()
    object_name = ' '.join(terms[0:-1]).lower()
    object_number = int(terms[-1])
    objects[object_name] = object_number


annopath = '/home/li/Dropbox/Generate-Polygon/annotations'
list_dirs = os.walk(annopath)
for root, dirs, files in list_dirs:
    for f in files:
        imName = os.path.splitext(f)[0]
        parse_annotation(imName,objects)

'''
parse_annotation('sun_awljsdrqhvfapzhf',objects)
im = Image.open('Parsed/sun_awljsdrqhvfapzhf.png')
im = np.array(im)
'''
