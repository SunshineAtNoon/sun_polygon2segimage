from shutil import copyfile
import os
src_path = '/home/li/Documents/Generate-Polygon/images'
dst_path = '/home/li/Documents/deep-mrf/imgs/train/raw_images'
seg_path = '/home/li/Documents/Generate-Polygon/Parsed'

list_dirs = os.walk(seg_path)
for root, dirs, files in list_dirs:
    for f in files:
        imName = os.path.splitext(f)[0]
        copyfile(src_path+'/'+imName+'.jpg',dst_path+'/'+imName+'.jpg')
