annopath = '/home/li/Documents/Generate-Polygon/annotations'
import os
from xml.dom import minidom

objs = set()
list_dirs = os.walk(annopath)
for root, dirs, files in list_dirs:
    for f in files:
        #ignore images bigger than 256*256 for now
        xmldoc = minidom.parse(os.path.join(root, f))
        itemlist = xmldoc.getElementsByTagName('object')
        for s in itemlist:
            nm = s.getElementsByTagName('name')
            objs.add(nm[0].childNodes[0].nodeValue)

obj_dict = {}
c = 0
for o in objs:
    if(o not in obj_dict):
        c = c + 1
        obj_dict[o] = c

print(obj_dict)
